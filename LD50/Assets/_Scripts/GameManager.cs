using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	#region Singleton
	public static GameManager Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] int width = 10;
	[SerializeField] int height = 6;

	public static Map CurrentMap { get; private set; }
	public static MapUnit PlayerUnit { get; private set; }
	public static bool IsInputAllowed { get; private set; } = true;
	public static bool InputBlockedByTutorial { get; set; } = true;

	public int TurnCounter { get; private set; } = 0;

	bool isGameOver = false;
	bool isInitialized = false;

	const float DEAD_ZONE = 0.1f;

	void Start()
	{
		Initialize();
		StartGame();
	}

	void Initialize()
	{
		ActionQueueVisual.Instance.Initialize();
		Score.Initialize();
		ScoreVisual.Instance.Initialize();

		isInitialized = true;
	}

	void Update()
	{
		if (isInitialized == false)
			return;

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

		// Check input
		if (IsInputAllowed && InputBlockedByTutorial == false && isGameOver == false)
        {
            bool _actionSuccess = CheckInput();
			if (_actionSuccess)
				IsInputAllowed = false;
		}
    }

	public void StartGame()
	{
		CurrentMap = new Map(width, height);
		MapVisual.Instance.GenerateMap(CurrentMap, 0.3f);

		ActionQueue.SetActions(new PlayerAction[]
		{
			new PlayerAction() { actionType = PlayerActionType.Move, weight = 0.7f },
			new PlayerAction() { actionType = PlayerActionType.Smack, weight = 1f },
			new PlayerAction() { actionType = PlayerActionType.Trip, weight = 0.5f },
			new PlayerAction() { actionType = PlayerActionType.Shoot, weight = 0.3f },
			new PlayerAction() { actionType = PlayerActionType.Block, weight = 0.3f }
		});

		Vector2Int _centerNode = CoordinateExtensions.GetCenter(width, height);
		Vector2Int _spawnCoords = CurrentMap.GetValidSpawnCoords(_centerNode);
		PlayerUnit = MapObjectGenerator.GeneratePlayer("Player", CurrentMap, _spawnCoords);
		PlayerHealthVisual.Instance.SetHealth(3, 3);
		CameraRack.Instance.SetTargetPos(PlayerUnit.ObjectVisual.transform.position);

		EnemyManager.Instance.GenerateEnemies(5, CurrentMap);

		IsInputAllowed = true;
	}

	bool CheckInput()
	{
		float _hor = Input.GetAxisRaw("Horizontal");
		float _ver = Input.GetAxisRaw("Vertical");

		if (Mathf.Abs(_hor + _ver) < DEAD_ZONE)
			return false;

		bool _actionSuccess = (Mathf.Abs(_hor) > Mathf.Abs(_ver))
			? PlayerActionManager.DoCurrentAction(new Vector2Int(Mathf.RoundToInt(Mathf.Sign(_hor)), 0), EndPlayerTurn)
			: PlayerActionManager.DoCurrentAction(new Vector2Int(0, Mathf.RoundToInt(Mathf.Sign(_ver))), EndPlayerTurn);

		return _actionSuccess;
	}

	#region Turn start/end
	void StartPlayerTurn()
	{
		TurnCounter++;
		IsInputAllowed = true;
		PlayerUnit.IsBlocking = false;
	}

	void EndPlayerTurn()
	{
		EnemyManager.Instance.ActiveEnemies.ForEach(x => x.IsBlocking = false);
		EnemyManager.Instance.DoEnemyActions(CurrentMap, PlayerUnit.CurrentCoordinates, StartPlayerTurn);
	}
    #endregion

    #region Game over/reset
    public void GameOver()
	{
		GameOverScreen.Instance.DoGameOver();
		isGameOver = true;
	}

	public void ResetGame()
	{
		Screenfade.Instance.Fade(false, 1f);
		StartCoroutine(CoroutineCollection.WaitForSeconds(1.1f, delegate
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}));
	}
    #endregion
}