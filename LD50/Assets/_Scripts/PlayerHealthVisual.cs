using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHealthVisual : MonoBehaviour
{
	#region Singleton
	public static PlayerHealthVisual Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] Image healthFill = null;
	[SerializeField] TMP_Text healthText = null;

	public void SetHealth(int _currHealth, int _maxHealth)
	{
		healthText.text = _currHealth + " / " + _maxHealth;
		healthFill.fillAmount = (float)_currHealth / _maxHealth;
	}
}