using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectGenerator : MonoBehaviour
{
	#region Singleton
	public static SoundEffectGenerator Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] AudioClip punch = null;
	[SerializeField] AudioClip miss = null;
	[SerializeField] AudioClip gunshot = null;
	[SerializeField] AudioClip block = null;

	AudioSource source;

	void Start()
	{
		source = GetComponent<AudioSource>();
	}

	public void PlaySound(SoundEffect _effect, float _delay = 0f)
	{
		//Debug.Log("@PlaySound, " + _effect.ToString() + ", " + _delay);

		StartCoroutine(CoroutineCollection.WaitForSeconds(_delay, delegate
		{
			switch (_effect)
			{
				case SoundEffect.None:
					break;
				case SoundEffect.Punch:
					source.PlayOneShot(punch);
					break;
				case SoundEffect.Miss:
					source.PlayOneShot(miss);
					break;
				case SoundEffect.Gunshot:
					source.PlayOneShot(gunshot);
					break;
				case SoundEffect.Block:
					source.PlayOneShot(block);
					break;
				default:
					break;
			}
		}));
	}
}

public enum SoundEffect { None = -1, Punch, Miss, Gunshot, Block }