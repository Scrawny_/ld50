using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ActionQueueVisual : MonoBehaviour
{
	#region Singleton
	public static ActionQueueVisual Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	TMP_Text textComponent;
	bool isInitialized = false;

	public void Initialize()
	{
		if (isInitialized)
			return;

		textComponent = GetComponentInChildren<TMP_Text>();
		ActionQueue.OnQueueMoved += RefreshText;

		isInitialized = true;
	}

	public void RefreshText()
	{
		if (isInitialized == false)
			return;

		string _text = "Action queue:\n<Size=130%>" + ActionToString(ActionQueue.CurrentAction) + "</size>\n";

		PlayerActionType[] _queue = ActionQueue.Queue;
		for (int i = 0; i < _queue.Length; i++)
		{
			_text += ActionToString(_queue[i]) + "\n";
		}

		textComponent.text = _text;
	}

	string ActionToString(PlayerActionType _action)
	{
		return _action.ToString();
	}
}