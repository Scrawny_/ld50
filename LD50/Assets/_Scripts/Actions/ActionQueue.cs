using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionQueue
{
    public static PlayerActionType CurrentAction { get; private set; }
    public static PlayerActionType[] Queue => actionQueue.ToArray();
    public static int QueueLength => DEFAULT_QUEUE_LENGTH;

    public static event System.Action OnQueueMoved;

    static Queue<PlayerActionType> actionQueue;
    static PlayerAction[] availableActions;
    static WeightedRandomize<PlayerActionType> actionRandomizer;

    const int DEFAULT_QUEUE_LENGTH = 3;

    public static void SetActions(PlayerAction[] _playerActions)
    {
        availableActions = _playerActions;
        actionRandomizer = new WeightedRandomize<PlayerActionType>(availableActions.ConvertAll(x => x.actionType), availableActions.ConvertAll(x => x.weight));

        actionQueue = new Queue<PlayerActionType>();
        for (int i = 0; i < QueueLength; i++)
        {
            actionQueue.Enqueue(GetRandomAction());
        }

        ActionQueueVisual.Instance.Initialize();

        MoveQueue();
    }

    public static void MoveQueue()
    {
        if (availableActions == null)
            return;

        CurrentAction = actionQueue.Dequeue();
        actionQueue.Enqueue(GetRandomAction());
    }

    static PlayerActionType GetRandomAction()
    {
        return actionRandomizer.GetRandom();
    }
}