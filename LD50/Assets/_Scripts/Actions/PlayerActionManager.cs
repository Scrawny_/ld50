using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerActionManager
{

	public static bool DoCurrentAction(Vector2Int _dir, System.Action _onFinished)
	{
		//Debug.Log("Doing " + ActionQueue.Instance.CurrentAction.ToString() + " to dir: " + _dir.ToString());

		PlayerActionType _currentAction = ActionQueue.CurrentAction;

		bool _success = false;
		switch (_currentAction)
		{
			case PlayerActionType.Move:
				if (HasViableMovementAvailable() == false)
					_success = DoTrip();
				else
					_success = DoMovement(_dir, GameManager.CurrentMap);
				break;
			case PlayerActionType.Smack:
				_success = DoPunch(_dir, GameManager.CurrentMap);
				break;
			case PlayerActionType.Trip:
				_success = DoTrip();
				break;
			case PlayerActionType.Shoot:
				_success = DoShoot(_dir, GameManager.CurrentMap);
				break;
			case PlayerActionType.Block:
				_success = DoBlock();
				break;
			default:
				break;
		}

		if (_success)
			ActionQueue.MoveQueue();

		return _success;
	}

    #region Do actions
    static bool DoMovement(Vector2Int _dir, Map _map)
	{
		Vector2Int _resultCoords = GameManager.PlayerUnit.CurrentCoordinates + _dir;
		if (_map.AreValidCoordinates(_resultCoords) == false)
			return false;

		MapNode _mapNode = _map.GetNode(_resultCoords);
		if (_mapNode.Inhabitant != null)
			return false;

		_map.MoveObject(GameManager.PlayerUnit, GameManager.PlayerUnit.CurrentCoordinates, _resultCoords);
		GameManager.PlayerUnit.ObjectVisual.SetPosition(_resultCoords, true);
		GameManager.PlayerUnit.ObjectVisual.LookDirection(_dir, 0.5f);

		CameraRack.Instance.SetTargetPos(_resultCoords.ToWorldPos());

		return true;
	}

	static bool DoTrip()
	{
		GameManager.PlayerUnit.ObjectVisual.TriggerAnimation("Trip");

		return true;
	}

	static bool DoBlock()
	{
		GameManager.PlayerUnit.ObjectVisual.TriggerAnimation("Block");
		GameManager.PlayerUnit.IsBlocking = true;

		return true;
	}

	static bool DoPunch(Vector2Int _dir, Map _map)
	{
		Vector2Int _resultCoords = GameManager.PlayerUnit.CurrentCoordinates + _dir;
		if (_map.AreValidCoordinates(_resultCoords) == false)
			return false;

		MapNode _mapNode = _map.GetNode(_resultCoords);
		if (_mapNode.Inhabitant != null)
		{
			_mapNode.Inhabitant.TakeDamage();
			SoundEffectGenerator.Instance.PlaySound(SoundEffect.Punch, 0.3f);
		}
		else
		{
			ParticleGenerator.Instance.DoEffect(_resultCoords.ToWorldPos(), ParticleEffect.Smoke);
			SoundEffectGenerator.Instance.PlaySound(SoundEffect.Miss, 0.3f);
		}

		GameManager.PlayerUnit.ObjectVisual.LookDirection(_dir, 0.4f);
		GameManager.PlayerUnit.ObjectVisual.TriggerAnimation("Attack");

		return true;
	}

	static bool DoShoot(Vector2Int _dir, Map _map)
	{
		Vector2Int _resultCoords = GameManager.PlayerUnit.CurrentCoordinates + _dir;

		bool _hit = false;
		bool _safety = false;
		while(_map.AreValidCoordinates(_resultCoords))
		{
			_safety = true;
			MapNode _mapNode = _map.GetNode(_resultCoords);
			if (_mapNode.Inhabitant != null)
			{
				_mapNode.Inhabitant.TakeDamage();
				_hit = true;
				break;
			}
			else
				_resultCoords += _dir;
		}

		if (_safety == false)
			return false;

		if (_hit == false)
			_resultCoords -= _dir;

		ParticleGenerator.Instance.DoEffect(_resultCoords.ToWorldPos(), ParticleEffect.Smoke);
		SoundEffectGenerator.Instance.PlaySound(SoundEffect.Gunshot, 0.3f);

		GameManager.PlayerUnit.ObjectVisual.LookDirection(_dir, 0.4f);
		GameManager.PlayerUnit.ObjectVisual.TriggerAnimation("Shoot");

		return true;
	}
    #endregion

    #region Helper functions
    static bool HasViableMovementAvailable()
    {
		List<Vector2Int> _neighbours = CoordinateExtensions.GetNeighbours(GameManager.PlayerUnit.CurrentCoordinates);
		int _viableNodeIndex = _neighbours.FindIndex(x => GameManager.CurrentMap.AreValidCoordinates(x) && GameManager.CurrentMap.GetNode(x).Inhabitant == null);

		return _viableNodeIndex != -1;
    }
    #endregion
}

public enum PlayerActionType { Move, Smack, Trip, Shoot, Block }