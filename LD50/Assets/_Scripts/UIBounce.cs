using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBounce : MonoBehaviour
{
	[SerializeField] AnimationCurve animCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
	[SerializeField] Vector2 movement = Vector2.right * 20f;
	[SerializeField] float animTime = 0.75f;

	float timer = 0f;
	Vector2 originalPos;

	void Start()
	{
		originalPos = transform.localPosition;
	}

	void Update()
	{
		timer += Time.deltaTime;
		while (timer >= animTime)
			timer -= animTime;

		Vector2 _movement = transform.right * movement.x + transform.up * movement.y;
		transform.localPosition = originalPos + _movement * animCurve.Evaluate(timer / animTime);
	}
}