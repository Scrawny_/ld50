using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutorial : MonoBehaviour
{
	#region Singleton
	public static Tutorial Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] CanvasGroup canvasGroup = null;

	Coroutine canvasRoutine;
	bool isTutActive = false;

	const string TUT_TAG = "hasPlayed";

	void OnEnable()
	{
		if (PlayerPrefs.HasKey(TUT_TAG) == false)
		{
			PlayerPrefs.SetInt(TUT_TAG, 1);
			canvasGroup.gameObject.SetActive(true);
			canvasRoutine = StartCoroutine(CoroutineCollection.WaitForSeconds(1f, delegate 
			{
				canvasRoutine = StartCoroutine(CoroutineCollection.DoOverTime(1.5f, delegate (float _phase)
				{
					isTutActive = true;
					canvasGroup.alpha = _phase;
				}));
			}));
		}
		else
		{
			GameManager.InputBlockedByTutorial = false;
			gameObject.SetActive(false);
		}
	}

	void Update()
	{
		if (isTutActive && Input.anyKeyDown)
		{
			if (canvasRoutine != null)
				StopCoroutine(canvasRoutine);

			float _startValue = canvasGroup.alpha;
			isTutActive = false;
			canvasRoutine = StartCoroutine(CoroutineCollection.DoOverTime(1f, delegate (float _phase)
			{
				canvasGroup.alpha = Mathf.Lerp(_startValue, 0f, _phase);
			}));

			StartCoroutine(CoroutineCollection.WaitForSeconds(2f, delegate
			{
				gameObject.SetActive(false);
				GameManager.InputBlockedByTutorial = false;
			}));
		}
	}

	[ContextMenu("Reset tutorial")]
	public void ResetTutorial()
	{
		if (PlayerPrefs.HasKey(TUT_TAG))
			PlayerPrefs.DeleteKey(TUT_TAG);
	}
}