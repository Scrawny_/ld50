using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject
{
    public MapObject(Vector2Int _spawnCoords, int _maxHealth)
    {
        CurrentCoordinates = _spawnCoords;

        MaxHealth = _maxHealth;
        CurrHealth = _maxHealth;
    }

    public Vector2Int CurrentCoordinates { get; set; }
    public MapObjectVisual ObjectVisual { get; set; }
    public int MaxHealth { get; set; }
    public int CurrHealth { get; set; }
    public bool IsBlocking { get; set; }

    public void TakeDamage(float _delay = 0f)
    {
        //Debug.Log("@TakeDamage");

        ParticleEffect _effect = ParticleEffect.Smoke;

        // If this is an unit, deal with score and blocking
        if (this is MapUnit _asMapUnit)
        {
            // Change particle effect to blood
            _effect = ParticleEffect.Blood;

            // If this is an enemy, add to score
            if (_asMapUnit.Faction == UnitFaction.Enemy)
            {
                Score.OnPlayerDealDamage(1);
                CurrHealth -= 1;
            }
            else if (_asMapUnit.Faction == UnitFaction.Player)
            {
                // If this is the player that isn't blocking, reduce score multiplier
                if (_asMapUnit.IsBlocking == false)
                {
                    CurrHealth -= 1;
                    PlayerHealthVisual.Instance.SetHealth(CurrHealth, MaxHealth);
                    Score.OnPlayerDamaged(1);
                }
                else
                {
                    _effect = ParticleEffect.Smoke;
                    SoundEffectGenerator.Instance.PlaySound(SoundEffect.Block, 0.3f);
                }
            }
        }
        else // For map objects, just take damage
            CurrHealth -= 1;

        ObjectVisual.StartCoroutine(CoroutineCollection.WaitForSeconds(0.3f + _delay, delegate
        {
            ParticleGenerator.Instance.DoEffect(ObjectVisual.transform.position, _effect);

            if (CurrHealth <= 0)
                Die();
        }));
    }

    virtual protected void Die()
    {
        ObjectVisual.KillVisual();
        GameManager.CurrentMap.GetNode(CurrentCoordinates).Uninhabit();
    }
}