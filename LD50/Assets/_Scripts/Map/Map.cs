using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
    public Map(int _width, int _height)
    {
        Width = _width;
        Height = _height;
        map = new MapNode[_width, _height];
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                map[x, y] = new MapNode(x, y);
            }
        }
    }

    public int Width { get; private set; }
    public int Height { get; private set; }

    MapNode[,] map;

    public void MoveObject(MapUnit _mapUnit, Vector2Int _fromCoords, Vector2Int _toCoords)
    {
        //Debug.Log("@MoveObject, " + _fromCoords.ToString() + " -> " + _toCoords.ToString());

        if (AreValidCoordinates(_fromCoords) == false || AreValidCoordinates(_toCoords) == false)
        {
            Debug.LogError("Coordinates OOB");
            return;
        }

        GetNode(_fromCoords).Uninhabit();

        MapNode _toNode = GetNode(_toCoords);
        if (_toNode.Inhabitant != null)
            Debug.LogWarning("????");
        else
            _toNode.Inhabit(_mapUnit);

        _mapUnit.CurrentCoordinates = _toCoords;
    }

    public MapNode GetNode(Vector2Int _coordinates)
    {
        if (AreValidCoordinates(_coordinates) == false)
            return null;

        return map[_coordinates.x, _coordinates.y];
    }

    public bool AreValidCoordinates(Vector2Int _coordinates) => !(_coordinates.x < 0 || _coordinates.x >= Width || _coordinates.y < 0 || _coordinates.y >= Height);

    public bool IsViableSpawnNode(Vector2Int _coordinates)
    {
        if (AreValidCoordinates(_coordinates) == false)
            return false;

        return map[_coordinates.x, _coordinates.y].Inhabitant == null;
    }

    public Vector2Int GetValidSpawnCoords(Vector2Int _targetCoords) => CoordinateExtensions.FindFirstViable(_targetCoords, IsViableSpawnNode);
}