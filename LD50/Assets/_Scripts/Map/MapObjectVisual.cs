using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectVisual : MonoBehaviour
{
	[SerializeField] int maxHealth = 1;
	[SerializeField] float moveTime = 1f;

	MapObject mapObject;
	EnemyHealthBar healthBar;
	Animator animator;

	public void Initialize(MapObject _object)
	{
		mapObject = _object;
		animator = GetComponentInChildren<Animator>();
		healthBar = GetComponentInChildren<EnemyHealthBar>();
		if (healthBar != null)
		{
			if (maxHealth > 1)
				healthBar.Initialize(maxHealth);
			else
				healthBar.gameObject.SetActive(false);
		}
	}

	public void SetPosition(Vector2Int _coordinates, bool _animated, float _randomDelay = 0f)
	{
		Vector3 _targetPos = _coordinates.ToWorldPos();
		if (_animated)
		{
			Vector3 _fromPos = transform.position;
			TriggerAnimation("Walk");
			StartCoroutine(CoroutineCollection.DoOverTime(moveTime, Random.Range(0f, _randomDelay), delegate (float _phase)
			{
				transform.position = Vector3.Lerp(_fromPos, _targetPos, _phase);
			}));
		}
		else
			transform.position = _targetPos;
	}

	public void TriggerAnimation(string _animation)
	{
		if (animator != null)
			animator.SetTrigger(_animation);
	}

	public void LookDirection(Vector2Int _dir, float _animTime)
	{
		if (_animTime > 0f)
		{
			Quaternion _fromRot = transform.rotation;/* Debug.Log("");*/
			Quaternion _toRot = Quaternion.LookRotation(new Vector3(_dir.x, 0f, _dir.y), Vector3.up);

			StartCoroutine(CoroutineCollection.DoOverTime(_animTime, delegate (float _phase)
			{
				transform.rotation = Quaternion.Lerp(_fromRot, _toRot, _phase);
			}));
		}
		else
			transform.LookAt(transform.position + new Vector3(_dir.x, 0f, _dir.y));
	}

	public void KillVisual()
	{
		//Debug.Log("@KillVisual");

		Destroy(gameObject);
	}
}