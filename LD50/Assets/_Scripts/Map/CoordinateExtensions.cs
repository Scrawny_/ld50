using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoordinateExtensions
{
    static readonly List<Vector2Int> allDirections = new List<Vector2Int>
    {
        new Vector2Int(0, 1),
        new Vector2Int(1, 1),
        new Vector2Int(1, 0),
        new Vector2Int(1, -1),
        new Vector2Int(0, -1),
        new Vector2Int(-1, -1),
        new Vector2Int(-1, 0),
        new Vector2Int(-1, 1)
    };
    static readonly List<Vector2Int> cardinalDirections = new List<Vector2Int>
    {
        new Vector2Int(0, 1),
        new Vector2Int(1, 0),
        new Vector2Int(0, -1),
        new Vector2Int(-1, 0)
    };

    const int MAP_VIABILITY_CHECK_NUM = 1000;

    public static List<Vector2Int> GetNeighbours(Vector2Int _center)
    {
        List<Vector2Int> _neighbours = new List<Vector2Int>();
        cardinalDirections.ForEach(x => _neighbours.Add(_center + x));

        return _neighbours;
    }

    public static Vector2Int FindFirstViable(Vector2Int _start, System.Predicate<Vector2Int> _predicate)
    {
        HashSet<Vector2Int> _checkedCoordinates = new HashSet<Vector2Int>();
        Queue<Vector2Int> _border = new Queue<Vector2Int>();
        _border.Enqueue(_start);
        int _checkCounter = 0;
        while(_border.Count > 0)
        {
            _checkCounter++;
            if (_checkCounter > MAP_VIABILITY_CHECK_NUM)
                break;

            Vector2Int _coordinates = _border.Dequeue();
            if (_predicate(_coordinates))
                return _coordinates;

            GetNeighbours(_coordinates).ForEach(x =>
            {
                if (_checkedCoordinates.Contains(x) == false)
                {
                    _border.Enqueue(x);
                    _checkedCoordinates.Add(_coordinates);
                }
            });
        }

        Debug.LogError("No viable node found");
        return default;
    }

    public static Vector2Int GetRandomCardinal()
    {
        return cardinalDirections[Random.Range(0, cardinalDirections.Count)];
    }

    public static Vector3 ToWorldPos(this Vector2Int _coordinates) => new Vector3(_coordinates.x * MapVisual.NODE_SIZE + (MapVisual.NODE_SIZE / 2f), 0f, _coordinates.y * MapVisual.NODE_SIZE + (MapVisual.NODE_SIZE / 2f));

    public static Vector2Int GetCenter(int _width, int _height) => new Vector2Int(Mathf.RoundToInt(_width / 2f), Mathf.RoundToInt(_height / 2f));

    public static Vector2Int GetDir(Vector2Int _from, Vector2Int _to) 
    {
        Vector2Int _dir = _to - _from;
        _dir.x = Mathf.Clamp(_dir.x, -1, 1);
        _dir.y = Mathf.Clamp(_dir.y, -1, 1);

        return _dir;
    }
}