using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MapVisual : MonoBehaviour
{
	#region Singleton
	public static MapVisual Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] GameObject nodePrefab = null;
	[Header("Debug")]
	[SerializeField] bool drawDebug = true;
	[SerializeField] GameObject debugTextPrefab = null;

	List<MapObject> obstacles = new List<MapObject>();

	public const float NODE_SIZE = 1f;
	const float NOISE_SCALE = 0.3f;
	
	public void GenerateMap(Map _map, float _obstaclesValue = 0.1f)
	{
		int _width = _map.Width;
		int _height = _map.Height;
		for (int x = 0; x < _width; x++)
		{
			for (int y = 0; y < _height; y++)
			{
				GameObject _obj = Instantiate(nodePrefab, transform);
				_obj.transform.position = new Vector2Int(x, y).ToWorldPos();// new Vector3(x * NODE_SIZE, 0f, y * NODE_SIZE);
			}
		}

		GenerateObstacles(_map, _width, _height, _obstaclesValue);

		if (drawDebug)
			GenerateDebugObjects(_width, _height);
	}

	public void DestroyMap()
	{
		MapObject[] _obstacles = obstacles.ToArray();
		for (int i = 0; i < _obstacles.Length; i++)
		{
			Destroy(_obstacles[i].ObjectVisual.gameObject);
		}

		obstacles.Clear();
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="_width"></param>
	/// <param name="_height"></param>
	/// <param name="_obstaclesValue">0f to 1f, higher value means more obstacles</param>
	void GenerateObstacles(Map _map, int _width, int _height, float _obstaclesValue)
	{
		float _xOffset = Random.Range(0f, 10000f);
		float _Offset = Random.Range(0f, 10000f);
		float _addRandom = MapObjectGenerator.Instance.AddRandom;
		for (int x = 0; x < _width; x++)
		{
			for (int y = 0; y < _height; y++)
			{
				float _noise = Mathf.PerlinNoise(x * NOISE_SCALE + _xOffset, y * NOISE_SCALE + _xOffset);
				_noise += Random.Range(-_addRandom, _addRandom);

				if (_noise <= _obstaclesValue)
				{
					MapObject _mapObj = MapObjectGenerator.GenerateObstacle(_map, _noise / _obstaclesValue, new Vector2Int(x, y));
				}
			}
		}
	}

	void GenerateDebugObjects(int _width, int _height)
	{
		for (int x = 0; x < _width; x++)
		{
			for (int y = 0; y < _height; y++)
			{
				float _noise = Mathf.PerlinNoise(x * NOISE_SCALE, y * NOISE_SCALE);

				GameObject _obj = Instantiate(debugTextPrefab, transform);
				_obj.transform.position = new Vector2Int(x, y).ToWorldPos();
				_obj.GetComponent<TMP_Text>().text = "[" + x + ", " + y + "]";
			}
		}
	}
}