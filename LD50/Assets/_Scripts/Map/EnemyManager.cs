using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
	#region Singleton
	public static EnemyManager Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	public List<MapUnit> ActiveEnemies => activeEnemies.ConvertAll(x => x.mapUnit);

	List<Enemy> activeEnemies = new List<Enemy>();
	int turnCounter = 0;
	int freqIncrease = 0;

	int[] freqIncreasesByTurn = new int[]
	{
		15, 40, 80, 100
	};

	const int DEFAULT_SPAWN_FREQ = 3;

	public void GenerateEnemies(int _num, Map _map)
	{
		for (int i = 0; i < _num; i++)
		{
			Vector2Int _spawnCoords = GenerateSpawnCoords(_map);
			MapUnit _unit = MapObjectGenerator.GenerateZombie("Zombie", 1, _map, _spawnCoords);

			activeEnemies.Add(new Enemy()
			{
				mapUnit = _unit,
				intention = Intention.None
			});
		}
	}

	public void DoEnemyActions(Map _map, Vector2Int _playerCoords, System.Action _onFinished)
	{
		Enemy[] _enemies = activeEnemies.ToArray();
		for (int i = 0; i < _enemies.Length; i++)
		{
			// Jank safety
			if (_enemies[i] == null || _enemies[i].mapUnit == null || activeEnemies.Contains(_enemies[i]) == false)
			{
				Debug.LogWarning("Sussy");
				continue;
			}

			Enemy _enemy = _enemies[i];
			if (_enemy.intention == Intention.Attack)
			{
				_enemy.mapUnit.ObjectVisual.TriggerAnimation("Attack");

				MapNode _node = _map.GetNode(_enemy.targetCoords);
				if (_node.Inhabitant != null && _node.Inhabitant is MapUnit _asMapunit)
				{
					float _randomTime = Random.Range(0f, 0.3f);
					_asMapunit.TakeDamage(_randomTime);
					SoundEffectGenerator.Instance.PlaySound(SoundEffect.Punch, _randomTime);
				}

				_enemy.targetCoords = default;
				_enemy.intention = Intention.None;

				Destroy(_enemy.warningTileRef);
			}
			else
			{
				// If we're within attacking distance, prepare an attack
				if (Vector2Int.Distance(_enemy.mapUnit.CurrentCoordinates, _playerCoords) == 1f)
				{
					_enemy.targetCoords = _playerCoords;
					_enemy.intention = Intention.Attack;
					_enemy.mapUnit.ObjectVisual.LookDirection(CoordinateExtensions.GetDir(_enemy.mapUnit.CurrentCoordinates, _enemy.targetCoords), 0.3f);
					_enemy.warningTileRef = WarningTiles.Instance.ShowWarning(_enemy.targetCoords, CoordinateExtensions.GetDir(_enemy.mapUnit.CurrentCoordinates, _enemy.targetCoords));
				}
				else // Just move towards player
				{
					if (Pathfinding.FindPathTowards(_enemy.mapUnit.CurrentCoordinates, _playerCoords, _map, out Vector2Int _moveToNode))
					{
						Vector2Int _oldCoords = _enemy.mapUnit.CurrentCoordinates;
						_map.MoveObject(_enemy.mapUnit, _enemy.mapUnit.CurrentCoordinates, _moveToNode);
						_enemy.mapUnit.ObjectVisual.SetPosition(_moveToNode, true, 0.3f);
						Vector2Int _dir = CoordinateExtensions.GetDir(_oldCoords, _enemy.mapUnit.CurrentCoordinates);
						_enemy.mapUnit.ObjectVisual.LookDirection(_dir, 0.5f);
					}
				}
			}
		}

		// Spawn more enemies
		turnCounter++;
		int _spawnFreq = DEFAULT_SPAWN_FREQ;
		for (int i = 0; i < freqIncreasesByTurn.Length; i++)
		{
			if (GameManager.Instance.TurnCounter >= freqIncreasesByTurn[i])
				_spawnFreq--;
		}

		if (turnCounter >= _spawnFreq)
		{
			int _numEnemies = 1;
			if (GameManager.Instance.TurnCounter >= 30)
				_numEnemies++;
			if (GameManager.Instance.TurnCounter >= 50)
				_numEnemies++;
			GenerateEnemies(_numEnemies, GameManager.CurrentMap);
			turnCounter = 0;

			//Debug.Log("Turn " + GameManager.Instance.TurnCounter + ", " + _numEnemies + " enemies");
		}
	}

	public void RemoveEnemy(MapUnit _enemyUnit)
	{
		Enemy _enemy = activeEnemies.Find(x => x.mapUnit == _enemyUnit);
		if (_enemy == null)
		{
			Debug.LogError("???");
			return;
		}

		if (_enemy.intention == Intention.Attack)
			Destroy(_enemy.warningTileRef);

		activeEnemies.Remove(_enemy);
	}

	Vector2Int GenerateSpawnCoords(Map _map)
	{
		Vector2Int _coords = new Vector2Int(Random.Range(0, _map.Width), Random.Range(0, _map.Height));
		return _map.GetValidSpawnCoords(_coords);
	}
}

class Enemy
{
	public MapUnit mapUnit;
	public Intention intention;
	public Vector2Int targetCoords;
	public GameObject warningTileRef;
}

enum Intention { None = -1, Attack }