using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNode
{
    public MapNode(int _x, int _y)
    {
        X = _x;
        Y = _y;
        Inhabitant = null;
    }

    public int X { get; private set; }
    public int Y { get; private set; }
    public MapObject Inhabitant { get; private set; }

    public void Inhabit(MapObject _mapObj)
    {
        Inhabitant = _mapObj;
    }

    public void Uninhabit()
    {
        Inhabitant = null;
    }
}