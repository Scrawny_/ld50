using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pathfinding
{

	public static bool FindPathTowards(Vector2Int _from, Vector2Int _to, Map _map, out Vector2Int _moveToNode)
	{
        HashSet<Vector2Int> _checkedCoordinates = new HashSet<Vector2Int>() { _from };
        Queue<Vector2Int> _border = new Queue<Vector2Int>();
        _border.Enqueue(_from);
        Dictionary<Vector2Int, Vector2Int> _cameFrom = new Dictionary<Vector2Int, Vector2Int>();

        InfiniteLoopCheck _check = new InfiniteLoopCheck(10000);
        bool _success = false;
        while (_border.Count > 0)
        {
            if (_check.IsLoopInfinite())
                break;

            Vector2Int _coordinates = _border.Dequeue();
            if (_coordinates == _to)
            {
                _success = true;
                break;
            }

            CoordinateExtensions.GetNeighbours(_coordinates).ForEach(x =>
            {
                if (_checkedCoordinates.Contains(x) == false && _map.AreValidCoordinates(x) && (x == _to || _map.GetNode(x).Inhabitant == null))
                {
                    _border.Enqueue(x);
                    _checkedCoordinates.Add(x);
                    _cameFrom[x] = _coordinates;
                }
            });
        }

        if (_success == false)
        {
            _moveToNode = default;
            return BruteForce(_from, _to, _map, out _moveToNode);
        }

        Vector2Int _prev = _to;
        Vector2Int _pointer = _to;
        while(_cameFrom.ContainsKey(_pointer))
        {
            if (_check.IsLoopInfinite())
                break;

            Vector2Int _coordinates = _cameFrom[_pointer];

            if (_coordinates == _to)
                break;

            _prev = _pointer;
            _pointer = _coordinates;
        }

        _moveToNode = _prev;
        return true;
    }

    static bool BruteForce(Vector2Int _from, Vector2Int _to, Map _map, out Vector2Int _moveToNode)
    {
        float _shortestDist = Vector2Int.Distance(_from, _to);
        bool _success = false;
        Vector2Int _bestCoords = default;
        CoordinateExtensions.GetNeighbours(_from).ForEach(x =>
        {
            if (_map.AreValidCoordinates(x) && _map.GetNode(x).Inhabitant == null)
            {
                float _dist = Vector2Int.Distance(x, _to);
                if (_dist <= _shortestDist)
                {
                    _bestCoords = x;
                    _shortestDist = _dist;
                    _success = true;
                }
            }
        });

        _moveToNode = _bestCoords;

        return _success;
    }
}