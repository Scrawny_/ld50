using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapUnit : MapObject
{
    public MapUnit(string _name, int _maxHealth, UnitFaction _faction, Vector2Int _coordinates) : base(_coordinates, _maxHealth)
    {
        MyName = _name;
        Faction = _faction;
        CurrentCoordinates = _coordinates;
    }

    public string MyName { get; private set; }
    public UnitFaction Faction { get; private set; } = UnitFaction.None;

    protected override void Die()
    {
        if (Faction == UnitFaction.Player)
        {
            GameManager.Instance.GameOver();
            return;
        }

        base.Die();
    }
}

public enum UnitFaction { None = -1, Player, Enemy }