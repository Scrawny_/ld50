using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectGenerator : MonoBehaviour
{
	#region Singleton
	public static MapObjectGenerator Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] GameObject playerPrefab = null;
	[SerializeField] GameObject zombiePrefab = null;
	[SerializeField] float addRandom = 0.1f;
	[SerializeField] AnimationCurve obstacleCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
	[SerializeField] GameObject[] obstaclePrefabs = default;

	public float AddRandom => addRandom;

	public static MapUnit GeneratePlayer(string _name, Map _map, Vector2Int _spawnCoords)
	{
		if (_map.IsViableSpawnNode(_spawnCoords) == false)
		{
			Debug.LogError("Not a viable spawn node: " + _spawnCoords.ToString());
			return null;
		}

		MapUnit _unit = new MapUnit(_name, 3, UnitFaction.Player, _spawnCoords);
		_map.GetNode(_spawnCoords).Inhabit(_unit);

		Instance.GenerateVisual(_unit, Instance.playerPrefab, _spawnCoords);

		return _unit;
	}

	public static MapUnit GenerateZombie(string _name, int _faction, Map _map, Vector2Int _spawnCoords)
	{
		if (_map.IsViableSpawnNode(_spawnCoords) == false)
		{
			Debug.LogError("Not a viable spawn node: " + _spawnCoords.ToString());
			return null;
		}

		MapUnit _unit = new MapUnit(_name, 1, UnitFaction.Enemy, _spawnCoords);
		_map.GetNode(_spawnCoords).Inhabit(_unit);

		Instance.GenerateVisual(_unit, Instance.zombiePrefab, _spawnCoords);

		ParticleGenerator.Instance.DoEffect(_spawnCoords.ToWorldPos(), ParticleEffect.Smoke);
		_unit.ObjectVisual.LookDirection(CoordinateExtensions.GetRandomCardinal(), 0f);

		return _unit;
	}

	public static MapObject GenerateObstacle(Map _map, float _obstacleValue, Vector2Int _spawnCoords)
	{
		if (_map.IsViableSpawnNode(_spawnCoords) == false)
		{
			Debug.LogError("Not a viable spawn node");
			return null;
		}

		MapObject _obstacle = new MapObject(_spawnCoords, 1);
		_map.GetNode(_spawnCoords).Inhabit(_obstacle);

		Instance.GenerateVisual(_obstacle, Instance.ObstacleValueToPrefab(_obstacleValue), _spawnCoords);
		_obstacle.ObjectVisual.LookDirection(CoordinateExtensions.GetRandomCardinal(), 0f);

		return _obstacle;
	}

	void GenerateVisual(MapObject _mapObject, GameObject _prefab, Vector2Int _spawnCoords)
	{
		MapObjectVisual _unitVisual = Instantiate(_prefab, transform).GetComponent<MapObjectVisual>();
		_mapObject.ObjectVisual = _unitVisual;
		_unitVisual.Initialize(_mapObject);

		_unitVisual.SetPosition(_spawnCoords, false);
	}

	GameObject ObstacleValueToPrefab(float _obstacleValue)
	{
		//Debug.Log("@ObstacleValueToPrefab, " + _obstacleValue);

		float _curveMod = obstacleCurve.Evaluate(_obstacleValue) - _obstacleValue;
		_obstacleValue += _curveMod; 

		float _swoosh = Mathf.Lerp(0f, obstaclePrefabs.Length - 0.0001f, _obstacleValue);
		int _index = Mathf.FloorToInt(_swoosh);

		return obstaclePrefabs[_index];
	}
}