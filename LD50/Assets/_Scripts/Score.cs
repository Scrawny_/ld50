using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score
{
	public static int CurrentScore => score;
	public static float CurrentMultiplier => multiplier;

	public static event System.Action OnScoreChanged;

	static bool wasDamageDealtThisTurn = false;
	static float multiplier = 0f;
	static int score = 0;
	static bool isInitialized = false;

	const float MULTIPLIER_DECAY = 0.7f;

	public static void Initialize()
    {
		ResetValues();
		isInitialized = true;
	}

	public static void OnPlayerDealDamage(int _damage)
	{
		if (isInitialized == false)
			return;

		multiplier += _damage;
		wasDamageDealtThisTurn = true;

		score += Mathf.RoundToInt(_damage * (1f + multiplier));
		ScoreVisual.Instance.UpdateText();
	}

	public static void OnPlayerDamaged(int _damage)
	{
		if (isInitialized == false)
			return;

		multiplier = Mathf.Max(multiplier - _damage, 0);

		ScoreVisual.Instance.UpdateText();
	}

	public static void OnPlayerTurnStart()
	{
		if (isInitialized == false)
			return;

		if (wasDamageDealtThisTurn == false)
			multiplier *= MULTIPLIER_DECAY;

		wasDamageDealtThisTurn = false;
	}

	static void ResetValues()
    {
		wasDamageDealtThisTurn = false;
		multiplier = 0f;
		score = 0;
		isInitialized = false;
	}
}