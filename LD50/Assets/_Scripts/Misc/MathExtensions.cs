﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MenuSystem
{
	public static class MathExtensions
	{
        /// <summary>
        /// Returns the value that is further away from 0f, positive or negative
        /// </summary>
		public static float MaxAbs(float _a, float _b)
		{
			if (_b.Abs() > _a.Abs())
				return _b;
			else 
				return _a;
		}

        /// <summary>
        /// Returns the absolute value
        /// </summary>
        /// <param name="_value"></param>
        /// <returns></returns>
		public static float Abs(this float _value)
        {
			return Mathf.Abs(_value);
        }

        /// <summary>
        /// Instead of value going from 0f to 1f, make it go from 0f to 1f to 0f. Rise then fall
        /// </summary>
        /// <param name="_offset">Offsetting 0.9f by 0.2f returns 0.1f</param>
        public static float SawToTriangle(float _value, float _offset = 0f)
        {
            return Mathf.Abs(1f - ((_value + _offset) % 1f) * 2f);
        }

        /// <summary>
        /// Modulo that also works with negative numbers. (-1, 3) == 2
        /// </summary>
        public static int BetterRemainder(int _value, int _mod)
        {
            return (_mod + (_value % _mod)) % _mod;
        }

        /// <summary>
        /// Modulo that also works with negative numbers. (-1, 3) == 2
        /// </summary>
        public static float BetterRemainder(float _value, float _mod)
        {
            return (_mod + (_value % _mod)) % _mod;
        }

        /// <summary>
        /// Regular Sign function, but doesn't consider 0 a positive number. 0f returns 0
        /// </summary>
        public static int BetterSign(float _value)
        {
            if (_value > 0f)
                return 1;
            else if (_value < 0f)
                return -1;
            else
                return 0;
        }

        /// <summary>
        /// Returns true if value is great than or equal to _min, and smaller than or equal to _max
        /// </summary>
        public static bool IsWithinRange(int _value, int _min, int _max)
        {
            return _value <= _max && _value >= _min;
        }

        /// <summary>
        /// Works like an ease-in or ease-out animation curve
        /// </summary>
        /// <param name="_value">Value from -1f to 1f</param>
        /// <param name="_bias">1f is complete ease-in, -1f is complete ease-out. 0f is 1:1</param>
        public static float BiasCurve(float _value, float _bias)
        {
            if (_bias == 1f && _value == 1f)
                return 1f;

            float _adjust = Mathf.Pow(1f - Mathf.Abs(_bias), 3);
            if (_bias >= 0)
                return (_value * _adjust) / (_value * _adjust - _value + 1f);
            else
                return (_value / (_value - _adjust * _value + _adjust));
        }

        public static int RoundToInt(this float _float)
        {
            return Mathf.RoundToInt(_float);
        }

        public static int SignToInt(this float _float)
        {
            return Mathf.RoundToInt(Mathf.Sign(_float));
        }

        public static int FloorToInt(this float _float)
        {
            return Mathf.FloorToInt(_float);
        }

        public static int FloorToInt(this double _float)
        {
            return Mathf.FloorToInt((float)_float);
        }

        public static int CeilToInt(this float _float)
        {
            return Mathf.CeilToInt(_float);
        }

        public static int RoundTowardsZero(this float _float)
        {
            return System.Math.Sign(_float) * Mathf.FloorToInt(Mathf.Abs(_float));
        }

        public static float MoveTowards(this float _float, float _towards, float _maxAmount)
        {
            if (_towards > _float)
                return Mathf.Min(_float + _maxAmount, _towards);
            else
                return Mathf.Max(_float - _maxAmount, _towards);
        }

        public static float GetSignedAngleBetween(float _tarAngle, float _currAngle)
        {
            float _value = _tarAngle - _currAngle;
            return BetterRemainder((_value + 180f), 360f) - 180f;
        }
    }
}