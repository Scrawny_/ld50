﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineCollection
{
	public static IEnumerator WaitForEndOfFrame(System.Action _action)
	{
		yield return new WaitForEndOfFrame();
		_action.Invoke();
	}

	public static IEnumerator WaitForSeconds(float _seconds, System.Action _action)
	{
		yield return new WaitForSeconds(_seconds);
		_action.Invoke();
	}

	public static IEnumerator DoOverTime(float _time, System.Action<float> _action)
	{
		float _timer = 0f;
		while (_timer < _time)
		{
			_action.Invoke(_timer / _time);
			_timer += Time.unscaledDeltaTime;
			yield return null;
		}

		_action.Invoke(1f);
	}

	public static IEnumerator DoOverTime(float _time, float _delay, System.Action<float> _action)
	{
		yield return new WaitForSecondsRealtime(_delay);

		float _timer = 0f;
		while (_timer < _time)
		{
			_action.Invoke(_timer / _time);
			_timer += Time.unscaledDeltaTime;
			yield return null;
		}

		_action.Invoke(1f);
	}
}