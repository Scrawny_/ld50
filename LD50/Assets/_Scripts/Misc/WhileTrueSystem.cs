﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhileTrueSystem
{
	public bool IsTrue
	{
		get
		{
			if (RESTRICT_TO_ONCE_PER_UPDATE == false || Time.time != lastRefresh)
				CheckFuncs();

			return whileTrueFuncs.Count > 0;
		}
	}

	readonly List<Func<bool>> whileTrueFuncs = new List<Func<bool>>();
	float lastRefresh = 0;

	const bool RESTRICT_TO_ONCE_PER_UPDATE = true;

	public void AddCheck(Func<bool> _whileTrueFunc)
	{
		if (_whileTrueFunc == null || _whileTrueFunc.Invoke() == false)
			return;

		whileTrueFuncs.Add(_whileTrueFunc);
	}

	void CheckFuncs()
	{
		//Debug.Log("@CheckFuncs, " + whileTrueFuncs.Count);

		Func<bool>[] _funcArray = whileTrueFuncs.ToArray();
		for (int i = 0; i < _funcArray.Length; i++)
		{
			if (_funcArray[i] == null || _funcArray[i].Invoke() == false)
				whileTrueFuncs.Remove(_funcArray[i]);
		}

		lastRefresh = Time.time;
	}

	public void ClearAllChecks()
	{
		whileTrueFuncs.Clear();
	}
}