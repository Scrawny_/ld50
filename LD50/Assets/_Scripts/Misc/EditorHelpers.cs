﻿#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;

public static class EditorHelpers
{
    public static void ClearDebugLog()
    {
        Assembly _assembly = Assembly.GetAssembly(typeof(ActiveEditorTracker));
        System.Type _type = _assembly.GetType("UnityEditor.LogEntries");
        MethodInfo _method = _type.GetMethod("Clear");
        _method.Invoke(new object(), null);
    }
}
#endif