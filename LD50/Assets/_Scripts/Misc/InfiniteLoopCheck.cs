﻿using UnityEngine;

public class InfiniteLoopCheck
{
    public InfiniteLoopCheck(int _maxLoops)
    {
        maxLoops = _maxLoops;
        loopCounter = 0;
    }

    int maxLoops;
    int loopCounter;

    public bool IsLoopInfinite()
    {
        loopCounter++;

        if (loopCounter >= maxLoops)
        {
            Debug.LogError("Infinite loop found");
            return true;
        }
        else
            return false;
    }
}