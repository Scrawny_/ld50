﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GizmoExtensions
{
	public static void DrawSquare(Vector3 _center, Vector2 _size, Vector3 _upDir)
	{
		Vector3 _rightDir = Vector3.zero;
		_rightDir.x = -_upDir.y;
		_rightDir.y = _upDir.x;

		Vector3 _bottomLeft =	_center - _rightDir * _size.x / 2f - _upDir * _size.y / 2f;
		Vector3 _topLeft =		_center - _rightDir * _size.x / 2f + _upDir * _size.y / 2f;
		Vector3 _bottomRight =	_center + _rightDir * _size.x / 2f - _upDir * _size.y / 2f;
		Vector3 _topRight =		_center + _rightDir * _size.x / 2f + _upDir * _size.y / 2f;

		Gizmos.DrawLine(_bottomLeft, _topLeft);
		Gizmos.DrawLine(_topLeft, _topRight);
		Gizmos.DrawLine(_topRight, _bottomRight);
		Gizmos.DrawLine(_bottomRight, _bottomLeft);
	}
}