using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEST : MonoBehaviour
{
#if UNITY_EDITOR
#region Singleton
	public static TEST Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
#endregion

	[SerializeField] bool testOnStart = true;
	[SerializeField] float timeScale = 1f;

	void Start()
	{
		if (timeScale != 1f)
			Time.timeScale = timeScale;

		if (testOnStart)
			DoStartTest();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.T))
			DoTest();
		if (Input.GetKeyDown(KeyCode.F1))
			EditorHelpers.ClearDebugLog();
	}

	void DoStartTest()
	{

	}

	[ContextMenu("Do test")]
	void DoTest()
	{
		
	}
#else
	void Start()
    {
		Destroy(gameObject);
    }
#endif
}