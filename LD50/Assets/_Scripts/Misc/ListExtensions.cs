﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static T GetFirst<T>(this List<T> _list)
    {
        if (_list == null || _list.Count == 0)
        {
            Debug.LogWarning("List either null or empty");
            return default;
        }

        return _list[0];
    }

    public static T GetFirst<T>(this T[] _list)
    {
        if (_list == null || _list.Length == 0)
        {
            Debug.LogWarning("List either null or empty");
            return default;
        }

        return _list[0];
    }

    public static T GetLast<T>(this List<T> _list)
    {
        if (_list == null || _list.Count == 0)
        {
            Debug.LogWarning("List either null or empty");
            return default;
        }

        return _list[_list.Count - 1];
    }

    public static T GetLast<T>(this T[] _list)
    {
        if (_list == null || _list.Length == 0)
        {
            Debug.LogWarning("List either null or empty");
            return default;
        }

        return _list[Mathf.Max(_list.Length - 1, 0)];
    }

    public static T GetAtClampedIndex<T>(this List<T> _list, int _index)
    {
        _index = Mathf.Clamp(_index, 0, _list.Count - 1);
        return _list[_index];
    }

    public static T GetRandom<T>(this List<T> _list)
    {
        if (_list == null || _list.Count == 0)
            return default;

        int _index = Random.Range(0, _list.Count);
        return _list[_index];
    }

    public static T GetRandom<T>(this T[] _array)
    {
        if (_array == null)
            return default;

        int _index = Random.Range(0, _array.Length);
        return _array[_index];
    }

    public static T[] GetRange<T>(this T[] _array, int _startIndex, int _length)
    {
        if (_array == null || _array.Length == 0)
        {
            Debug.LogWarning("List either null or empty");
            return default;
        }

        _startIndex = Mathf.Clamp(_startIndex, 0, _array.Length - 1);
        _length = Mathf.Clamp(_length, 1, _array.Length - _startIndex);

        T[] _returnValue = new T[_length];
        for (int i = 0; i < _length; i++)
        {
            _returnValue[i] = _array[i + _startIndex];
        }

        return _returnValue;
    }

    public static T GetRandomWeighted<T>(this T[] _array, System.Func<T, float> _weightFunc)
    {
        if (_array == null || _array.Length == 0)
        {
            Debug.LogWarning("Array either null or empty");
            return default;
        }

        float _totalValue = 0f;
        _array.ForEach(x => _totalValue += _weightFunc(x));

        float _random = Random.Range(0f, _totalValue);
        float _valuePointer = 0f;
        for (int i = 0; i < _array.Length; i++)
        {
            _valuePointer += _weightFunc(_array[i]);
            if (_valuePointer > _random)
                return _array[i];
        }

        if (_random != _totalValue)
            Debug.LogWarning("Weight function didn't find anything");

        return _array.GetLast();
    }

    public static T[] GetRandoms<T>(this List<T> _array, int _num)
    {
        T[] _randoms = new T[_num];
        for (int i = 0; i < _randoms.Length; i++)
        {
            _randoms[i] = _array.GetRandom();
        }

        return _randoms;
    }

    public static T[] ResizeArray<T>(this T[] _array, int _targetLength)
    {
        //Debug.Log("@ResizeArray, " + _array.Length + " => " + _targetLength);

        T[] _extendedArray = new T[_targetLength];

        for (int i = 0; i < _targetLength; i++)
        {
            if (i >= _array.Length)
                break;

            _extendedArray[i] = _array[i];
        }

        return _extendedArray;
    }

    public static T[] AddRange<T>(this T[] _array, T[] _add)
    {
        T[] _extendedArray = new T[_array.Length + _add.Length];
        for (int i = 0; i < _extendedArray.Length; i++)
        {
            if (i < _array.Length)
                _extendedArray[i] = _array[i];
            else
                _extendedArray[i] = _add[i - _array.Length];
        }

        return _extendedArray;
    }

    public static T[] AddRange<T>(this T[] _array, List<T> _add)
    {
        T[] _extendedArray = new T[_array.Length + _add.Count];
        for (int i = 0; i < _extendedArray.Length; i++)
        {
            if (i < _array.Length)
                _extendedArray[i] = _array[i];
            else
                _extendedArray[i] = _add[i - _array.Length];
        }

        return _extendedArray;
    }

    public static T[] RemoveEmptyItems<T>(this T[] _array)
    {
        //Debug.Log("@RemoveEmptyItems, " + _array.Length);

        List<T> _modifiedArray = new List<T>();
        for (int i = 0; i < _array.Length; i++)
        {
            if (_array[i] != null)
                _modifiedArray.Add(_array[i]);
        }

        return _modifiedArray.ToArray();
    }

    public static T[] RemoveAt<T>(this T[] _array, int _index)
    {
        //Debug.Log("@Remove, " + _array.Length);

        List<T> _modifiedArray = new List<T>(_array);
        _modifiedArray.RemoveAt(_index);
        return _modifiedArray.ToArray();
    }

    public static T[] Remove<T>(this T[] _array, T _item)
    {
        //Debug.Log("@Remove, " + _array.Length);

        List<T> _modifiedArray = new List<T>(_array);
        _modifiedArray.Remove(_item);
        return _modifiedArray.ToArray();
    }

    public static T[] FillWith<T>(this T[] _array, T _item)
    {
        for (int i = 0; i < _array.Length; i++)
        {
            _array[i] = _item;
        }

        return _array;
    }

    public static void ForEach<T>(this T[] _array, System.Action<T> _action)
    {
        for (int i = 0; i < _array.Length; i++)
        {
            _action.Invoke(_array[i]);
        }
    }

    public static List<T> FindAll<T>(this T[] _array, System.Predicate<T> _predicate)
    {
        List<T> _foundItems = new List<T>();
        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
                _foundItems.Add(_array[i]);
        }

        return _foundItems;
    }

    public static List<int> FindAllIndexes<T>(this T[] _array, System.Predicate<T> _predicate)
    {
        List<int> _viableIndexes = new List<int>();
        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
                _viableIndexes.Add(i);
        }

        return _viableIndexes;
    }

    public static List<int> FindAllIndexes<T>(this List<T> _array, System.Predicate<T> _predicate)
    {
        List<int> _viableIndexes = new List<int>();
        for (int i = 0; i < _array.Count; i++)
        {
            if (_predicate.Invoke(_array[i]))
                _viableIndexes.Add(i);
        }

        return _viableIndexes;
    }

    public static int FindIndex<T>(this T[] _array, System.Predicate<T> _predicate)
    {
        if (_array == null)
        {
            Debug.LogError("Null array");
            return default;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
                return i;
        }

        return -1;
    }

    public static bool FindAndDo<T>(this T[] _array, System.Predicate<T> _predicate, System.Action<T> _doIfFound)
    {
        if (_array == null)
        {
            Debug.LogError("Null array");
            return false;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
            {
                _doIfFound.Invoke(_array[i]);
                return true;
            }
        }

        return false;
    }

    public static void FindAllAndDo<T>(this T[] _array, System.Predicate<T> _predicate, System.Action<T> _doIfFound)
    {
        if (_array == null)
        {
            Debug.LogError("Null array");
            return;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
                _doIfFound.Invoke(_array[i]);
        }
    }

    public static bool TrueForAll<T>(this T[] _array, System.Predicate<T> _predicate)
    {
        if (_array == null)
        {
            Debug.LogError("Null array");
            return false;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]) == false)
                return false;
        }

        return true;
    }

    public static T Find<T>(this T[] _array, System.Predicate<T> _predicate)
    {
        if (_array == null)
        {
            Debug.LogError("Null array");
            return default;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_predicate.Invoke(_array[i]))
                return _array[i];
        }

        return default;
    }

    public static List<T> ToList<T>(this IEnumerable<T> _list)
    {
        if (_list == null)
            return new List<T>();

        return new List<T>(_list);
    }

    public static List<T> ToList<T>(this T[,] _array)
    {
        if (_array == null)
        {
            Debug.LogWarning("Null array");
            return new List<T>();
        }

        List<T> _list = new List<T>();
        int _xLength = _array.GetLength(0);
        for (int x = 0; x < _xLength; x++)
        {
            int _yLength = _array.GetLength(1);
            for (int y = 0; y < _yLength; y++)
            {
                _list.Add(_array[x, y]);
            }
        }

        return _list;
    }

    public static Queue<T> ToQueue<T>(this IEnumerable<T> _list)
    {
        return new Queue<T>(_list);
    }

    public static HashSet<T> ToHashSet<T>(this IEnumerable<T> _array)
    {
        return new HashSet<T>(_array);
    }

    public static T[][] ConvertToGrid<T>(this T[] _array, int _perRow) // Untested
    {
        //Debug.Log("@ConvertToGrid, " + _array.Length + ", " + _perRow);

        T[][] _grid = new T[_perRow][];

        int _index = 0;
        int _height = Mathf.CeilToInt((float)_array.Length / _perRow);
        for (int y = 0; y < _height; y++)
        {
            _grid[y] = new T[_perRow];
            for (int x = 0; x < _perRow; x++)
            {
                if (_index >= _array.Length)
                    continue;

                _grid[y][x] = _array[_index];

                _index++;
            }
        }

        return _grid;
    }

    public static bool Contains<T>(this T[] _array, T _item)
    {
        if (_item == null)
        {
            Debug.LogWarning("Null item");
            return false;
        }
        if (_array == null)
        {
            Debug.LogWarning("Null array");
            return false;
        }

        for (int i = 0; i < _array.Length; i++)
        {
            if (_array[i].Equals(_item))
                return true;
        }

        return false;
    }

    public static bool ContainsKey<T1, T2>(this Dictionary<T1, T2> _dictionary, T1[] _keys)
    {
        for (int i = 0; i < _keys.Length; i++)
        {
            if (_dictionary.ContainsKey(_keys[i]))
                return true;
        }

        return false;
    }

    public static T2[] ConvertAll<T1, T2>(this T1[] _array, System.Converter<T1, T2> _converter)
    {
        T2[] _converted = new T2[_array.Length];
        for (int i = 0; i < _array.Length; i++)
        {
            _converted[i] = _converter(_array[i]);
        }

        return _converted;
    }

    public static bool IsOutOfBounds<T>(this List<T> _list, int _index)
    {
        return _index < 0 || _index >= _list.Count;
    }

    public static bool IsWithinBounds<T>(this T[] _list, int _index)
    {
        return _index < 0 || _index >= _list.Length;
    }

    public static List<T> Clone<T>(this List<T> _list)
    {
        return new List<T>(_list);
    }

    public static string[] Sort(this string[] _array)
    {
        if (_array == null)
        {
            Debug.LogWarning("Null array");
            return _array;
        }

        List<string> _list = new List<string>(_array);
        _list.Sort();

        return _list.ToArray();
    }

    public static T[] Add<T>(this T[] _array, T _item)
    {
        T[] _newArray = new T[_array.Length + 1];
        for (int i = 0; i < _array.Length; i++)
        {
            _newArray[i] = _array[i];
        }

        _newArray[_newArray.Length - 1] = _item;

        return _newArray;
    }

    public static Dictionary<T1, T2> Clone<T1, T2>(this Dictionary<T1, T2> _dictionary)
    {
        Dictionary<T1, T2> _cloneDictionary = new Dictionary<T1, T2>();
        List<T1> _keys = new List<T1>(_dictionary.Keys);
        for (int i = 0; i < _keys.Count; i++)
        {
            _cloneDictionary.Add(_keys[i], _dictionary[_keys[i]]);
        }

        return _cloneDictionary;
    }

    public static List<T> RemoveDulicates<T>(this List<T> _list)
    {
        List<T> _uniques = new List<T>();
        for (int i = 0; i < _list.Count; i++)
        {
            if (_uniques.Contains(_list[i]) == false)
                _uniques.Add(_list[i]);
        }

        return _uniques;
    }

    public static int CalculateTotal(this List<int> _list)
    {
        int _total = 0;
        _list.ForEach(x => _total += x);

        return _total;
    }

    public static float CalculateAverage(this List<float> _list)
    {
        float _total = 0;
        _list.ForEach(x => _total += x);

        return _total / _list.Count;
    }

    public static string ToDebugString<T>(this IEnumerable<T> _list)
    {
        string _returnValue = "[";
        List<string> _texts = _list.ToList().ConvertAll(x => (x != null) ? x.ToString() : "NULL");
        for (int i = 0; i < _texts.Count; i++)
        {
            if (i != 0)
                _returnValue += ", ";

            _returnValue += _texts[i];
        }
        _returnValue += "]";

        return _returnValue;
    }

    public static void DebugAsStrings<T>(this IEnumerable<T> _list)
    {
        string _returnValue = "[";
        List<string> _texts = _list.ToList().ConvertAll(x => (x != null) ? x.ToString() : "NULL");
        for (int i = 0; i < _texts.Count; i++)
        {
            if (i != 0)
                _returnValue += ", ";

            _returnValue += _texts[i];
        }
        _returnValue += "]";

        Debug.Log(_returnValue);
    }

    public static void DebugAsString(this object _object)
    {
        Debug.Log(_object.ToString());
    }
}