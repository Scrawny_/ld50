using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreVisual : MonoBehaviour
{
	#region Singleton
	public static ScoreVisual Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] TMP_Text multiplierText = null;

	TMP_Text textComponent;
	bool isInitialized = false;

	public void Initialize()
	{
		textComponent = GetComponentInChildren<TMP_Text>();
		Score.OnScoreChanged += UpdateText;
		isInitialized = true;
	}

	public void UpdateText()
	{
		if (isInitialized == false)
			return;

		multiplierText.text = "Multiplier: " + (1f + Score.CurrentMultiplier).ToString("F1") + "\n" + MultiplierToRank(Score.CurrentMultiplier);
		textComponent.text = "Score: " + Score.CurrentScore;
	}

	string MultiplierToRank(float _multiplier)
	{
		if (_multiplier == 0f)
			return "";
		else if (_multiplier < 0.5f)
			return " (<size=110%>D</size>amp)";
		else if (_multiplier < 1f)
			return " (<size=120%>C</size>onsiderable)";
		else if (_multiplier < 2f)
			return " (<size=130%>B</size>ashful)";
		else if (_multiplier < 3f)
			return " (<size=140%>A</size>liven't)";
		else
			return " (<size=150%>SSS</size>ubstantial)";
	}
}