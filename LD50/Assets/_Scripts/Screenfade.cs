using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screenfade : MonoBehaviour
{
	#region Singleton
	public static Screenfade Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] Image image = null;
	[SerializeField] AnimationCurve slideCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	void OnEnable()
	{
		image.gameObject.SetActive(true);
		image.fillAmount = 1f;

		float _delay = 0.3f;
#if UNITY_EDITOR
		_delay = 2f;
#endif

		StartCoroutine(CoroutineCollection.WaitForSeconds(_delay, delegate
		{
			Fade(true, 1f);
		}));
	}

	public void Fade(bool _in, float _time)
	{
		RandomizeWipe();
		StartCoroutine(CoroutineCollection.DoOverTime(_time, delegate (float _phase)
		{
			image.fillAmount = slideCurve.Evaluate(_in ? 1f - _phase : _phase);
		}));
	}

	void RandomizeWipe()
	{
		image.fillMethod = (Image.FillMethod)Random.Range(0, 5);
		image.fillClockwise = Random.Range(0, 2) == 1;
		image.fillOrigin = Random.Range(0, 4);
	}
}