using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
	#region Singleton
	public static EnemyHealthBar Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] GameObject healthCubePrefab = null;
	[SerializeField] Color spentColor = Color.white;
	
	int maxHealth;
	int currHealth;
	MeshRenderer[] healthCubes;

	const float PADDING = 0.1f;

	public void Initialize(int _maxHealth)
	{
		maxHealth = _maxHealth;
		currHealth = _maxHealth;

		float _size = healthCubePrefab.transform.localScale.x;
		float _totalSpace = _size * _maxHealth + PADDING * (_maxHealth - 1);
		float _spacePerCube = _size + PADDING;

		Vector3 _startPos = Vector3.left * _totalSpace / 2f;
		healthCubes = new MeshRenderer[_maxHealth];
		for (int i = 0; i < _maxHealth; i++)
		{
			healthCubes[i] = Instantiate(healthCubePrefab, transform).GetComponent<MeshRenderer>();
			healthCubes[i].transform.localPosition = _startPos + Vector3.right * i * _spacePerCube;
			healthCubes[i].gameObject.SetActive(true);
		}
	}

	public void TakeDamage()
	{
		if (currHealth <= 0)
			return;

		healthCubes[currHealth - 1].material.color = spentColor;
		currHealth--;
	}
}