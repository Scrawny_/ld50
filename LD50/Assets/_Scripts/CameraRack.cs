using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRack : MonoBehaviour
{
	#region Singleton
	public static CameraRack Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] float lerpTime = 0.6f;
	[SerializeField] AnimationCurve moveCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public void SetTargetPos(Vector3 _pos)
	{
		Vector3 _from = transform.position;
		Vector3 _to = _pos;

		StartCoroutine(CoroutineCollection.DoOverTime(lerpTime, delegate (float _phase)
		{
			transform.position = Vector3.Lerp(_from, _to, moveCurve.Evaluate(_phase));
		}));
	}
}