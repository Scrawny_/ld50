using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverScreen : MonoBehaviour
{
	#region Singleton
	public static GameOverScreen Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] GameObject panel = null;
	[SerializeField] TMP_Text textComp = null;
	[SerializeField] CanvasGroup uiGroup = null;

	bool isWaitingForInput = false;
	float startAcceptingInput = 0f;

	void Update()
	{
		if (isWaitingForInput && Time.time >= startAcceptingInput)
		{
			if (Input.anyKeyDown)
			{
				GameManager.Instance.ResetGame();
			}
		}
	}

	public void DoGameOver()
	{
		startAcceptingInput = Time.time + 1f;
		uiGroup.alpha = 0.3f;
		isWaitingForInput = true;
		panel.SetActive(true);
		textComp.text = "Final score: " + Score.CurrentScore;
	}
}