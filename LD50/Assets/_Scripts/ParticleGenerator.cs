using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleGenerator : MonoBehaviour
{
	#region Singleton
	public static ParticleGenerator Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] ParticleSystem blood = null;
	[SerializeField] ParticleSystem smoke = null;

	public void DoEffect(Vector3 _pos, ParticleEffect _effect)
	{
		ParticleSystem _system = null;
		switch (_effect)
		{
			case ParticleEffect.None:
				break;
			case ParticleEffect.Blood:
				_system = Instantiate(blood.gameObject, transform).GetComponent<ParticleSystem>();
				break;
			case ParticleEffect.Smoke:
				_system = Instantiate(smoke.gameObject, transform).GetComponent<ParticleSystem>();
				break;
			default:
				break;
		}

		_system.transform.position = _pos;
		_system.gameObject.SetActive(true);

		StartCoroutine(CoroutineCollection.WaitForSeconds(2f, delegate { Destroy(_system.gameObject); }));
	}
}

public enum ParticleEffect { None = -1, Blood, Smoke }