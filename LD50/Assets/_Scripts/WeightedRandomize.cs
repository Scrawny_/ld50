using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightedRandomize<T>
{
    public WeightedRandomize(T[] _items, float[] _weights)
    {
        items = _items;
        weights = _weights;
        totalWeight = 0f;
        for (int i = 0; i < items.Length; i++)
        {
            totalWeight += _weights[i];
        }
    }

    T[] items;
    float[] weights;
    float totalWeight;

    public T GetRandom()
    {
        float _value = Random.Range(0f, totalWeight);
        float _counter = 0f;
        for (int i = 0; i < weights.Length; i++)
        {
            _counter += weights[i];

            if (_value <= _counter)
                return items[i];
        }

        return default;
    }
}