using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningTiles : MonoBehaviour
{
	#region Singleton
	public static WarningTiles Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
	#endregion

	[SerializeField] GameObject tilePrefab = null;

	public GameObject ShowWarning(Vector2Int _coordinates, Vector2Int _dir)
	{
		return InstantiateTile(_coordinates, _dir);
	}

	GameObject InstantiateTile(Vector2Int _coordinates, Vector2Int _dir)
	{
		//Debug.Log("@InstantiateTile, " + _dir.ToString());

		GameObject _obj = Instantiate(tilePrefab, transform);
		_obj.transform.position = _coordinates.ToWorldPos();
		_obj.transform.LookAt(_obj.transform.position + Vector3.forward * _dir.y + Vector3.right * _dir.x);

		return _obj;
	}
}