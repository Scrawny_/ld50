﻿using UnityEngine;
using UnityEditor;

public class EditorScripts : EditorWindow
{
    const int TARGET_FPS = 5;

    [MenuItem("Editor Helper/Force low FPS")]
    static void ForceLowFPS()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = TARGET_FPS;
    }

    [MenuItem("Editor Helper/Reset PlayerPrefs")]
    static void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll(); 
        Debug.Log("Player prefs cleared");
    }

    [MenuItem("Editor Helper/Force project recompile")]
    static void ForceProjectRecompile()
    {
        Debug.Log("Recompile requested");
        UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
    }
}