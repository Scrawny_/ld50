-- BUGS ---
>

-- TODO ---
> Scale-based animations

-- DESIGN ---
> Actual entrance/exits? What does getting to the exit do?
	- Just optional routes? Skip the combat/rewards for this room?
> Pick which zone to move to next
	- different paths lead to different unlocks
	- need some skills or items to progress further in some directions (red key for red door etc)
	- start from the cellar, enter house, then pick directions. Town, woods, towards castle, cave, etc
	- collect resources and purchase defences for the early zones, so you own them. Don't need to fight, just pass
	  through and pick up upgrades. They also unlock stuff like the blacksmith
> Reset: you feel as though an infinitely thin layer of your soul has been shaved off. You wake up at blaablaa
> Keep it simple, stupid. Simple skills, small numbers
> Upgrade path: each new character bought also adds global stat upgrades, and upgrading characters
  also adds more global upgrades
> Stacking upgrades vs unique:
	- Stacking: unlocking more upgrades would make your runs worse 'cause less chance of stacking
		- Just make unlocks better, and don't have too many of them?
> Classes:
	- Warrior: basic, move, block, attack. Upgrades later to something better
	- Mage: strike nearest enemy (random, or prioritize attacking? Upgrade?)
	- Thief: movement dash through enemies, mug resources
	- Immobile: Anima, endgame/secret unlock?
	- Dragoon: move back when attacking, jump AoE (2 turn)
	- Monk: after attacking, same node explodes again on the next turn. Multi-directional attacks?
	- Greatsword: attacks back/front, cleave 3 nodes in direction

-- THEME ---
> Escaping flatland
	- start with flat camera, imitate a 2D game
	- see a cube cut into flatland space, touch to escape to third dimension
	- as camera pans, fade from chiptune to bigger tunes
	- enter the 4th dimension